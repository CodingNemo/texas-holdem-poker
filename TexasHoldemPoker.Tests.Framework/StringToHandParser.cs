﻿using System;
using System.Linq;
using TexasHoldemPoker.Hands;

namespace TexasHoldemPoker.Tests.Framework
{
    public class StringToHandParser
    {
        private readonly StringToCardParser _cardParser;

        public StringToHandParser(StringToCardParser cardParser)
        {
            _cardParser = cardParser;
        }

        public Hand Parse(string handAsString)
        {
            var cardsAsString = handAsString.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            var cards = cardsAsString
                .Select(_cardParser.Parse)
                .ToList();
            var hand = new Hand((short) cards.Count);
            cards.ForEach(c => hand.AddCard(c));
            return hand;
        }
    }
}