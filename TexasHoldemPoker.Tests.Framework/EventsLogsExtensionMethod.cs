﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NFluent;
using NFluent.Extensibility;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tests.Framework
{
    public static class EventsLogsExtensionMethod
    {
        public static ICheckLink<ICheck<IEnumerable>> StartsWith(
            this ICheck<IEnumerable<DomainEvent>> check,
            DomainEvent[] expectedStartOfLogs,
            int threshold = 0)
        {
            var runnableCheck = ExtensibilityHelper.ExtractChecker(check);
            var fullLog = runnableCheck.Value;
            var domainEvents = fullLog.ToList();

            var numberOfEventsToTake = expectedStartOfLogs.Length;
            var actualStartOfLogs = domainEvents.Take(numberOfEventsToTake);
            return Check.That(actualStartOfLogs).ContainsExactly(expectedStartOfLogs);
        }

        public static ICheckLink<ICheck<IEnumerable>> EndsWith(
            this ICheck<IEnumerable<DomainEvent>> check,
            DomainEvent[] expectedEndOfLogs,
            int threshold = 0)
        {
            var runnableCheck = ExtensibilityHelper.ExtractChecker(check);
            var fullLog = runnableCheck.Value;

            var domainEvents = fullLog.ToList();

            var numberOfEventsToSkip =
                threshold == 0 ? domainEvents.Count - expectedEndOfLogs.Length : threshold;

            var actualEndOfLogs = domainEvents.Skip(numberOfEventsToSkip);

            return Check.That(actualEndOfLogs).ContainsExactly(expectedEndOfLogs);
        }
    }
}