﻿using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tests.Framework.ScenarioGeneration
{
    public class Step
    {
        private readonly string stepText;
        public static Step FromType<TType>()
        {
            return new Step($"a {typeof(TType).Name}");
        }

        public static Step FromString(string text)
        {
            return new Step(text);
        }

        public static Step FromInstance(object instance)
        {
            return new Step(instance.ToReadableSentence());
        }

        private Step(string stepText)
        {
            this.stepText = stepText;
        }

        public override string ToString()
        {
            return stepText;
        }
    }
}
