﻿using System;

namespace TexasHoldemPoker.Tests.Framework
{
    public sealed class CardParsingPattern<TExpectedValue>
    {
        private CardParsingPattern(Predicate<string> condition, Func<string, TExpectedValue> getValue)
        {
            this.Condition = condition;
            GetValue = getValue;
        }

        private Predicate<string> Condition { get; }

        public Func<string, TExpectedValue> GetValue { get; }

        public static CardParsingPattern<TExpectedValue> Constant(string expectedValue, TExpectedValue value)
        {
            return new CardParsingPattern<TExpectedValue>(c => c.ToLower().Equals(expectedValue.ToLower()), c => value);
        }

        public static CardParsingPattern<TExpectedValue> Predicate(Predicate<string> condition,
            Func<string, TExpectedValue> getValue)
        {
            return new CardParsingPattern<TExpectedValue>(condition, getValue);
        }

        public bool Matches(string val)
        {
            return Condition(val);
        }
    }
}