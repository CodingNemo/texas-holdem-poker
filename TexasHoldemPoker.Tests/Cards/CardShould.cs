﻿using NFluent;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Cards.Rank;
using static TexasHoldemPoker.Cards.Suit;

namespace TexasHoldemPoker.Tests.Cards
{
    public class CardShould
    {
        [Fact]
        public void Be_Equal_To_Another_Card_With_The_Same_Rank_And_Suit()
        {
            Check.That(Eight.Of(Club))
                .Is(Eight.Of(Club));
        }


        [Fact]
        public void Not_Be_Equal_To_Another_Card_With_A_Different_Rank_And_Suit()
        {
            Check.That(Eight.Of(Club))
                .Not.Is(Seven.Of(Club));
        }
    }
}