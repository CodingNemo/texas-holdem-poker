﻿using System;
using System.Linq;
using NFluent;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Cards.Rank;
using static TexasHoldemPoker.Cards.Suit;

namespace TexasHoldemPoker.Tests.Cards
{
    public class DeckShould
    {
        public DeckShould()
        {
            _deck = new Deck();
        }

        private readonly Deck _deck;

        [Fact]
        public void Find_A_Card_By_Its_Rank_And_Suit()
        {
            foreach (var rank in Deck.Ranks)
                foreach (var suit in Deck.Suits)
                {
                    var card = _deck.FindCard(rank, suit);

                    Check.ThatEnum(card.Rank).IsEqualTo(rank);
                    Check.ThatEnum(card.Suit).IsEqualTo(suit);
                }
        }

        [Fact]
        public void Reset_The_Count_Of_Drawn_Cards_When_Shuffled()
        {
            _deck.DrawTopCard(_deck.Top);
            _deck.DrawTopCard(_deck.Top);

            _deck.Shuffle();

            Check.That(_deck.DrawnCardsCount).IsEqualTo(0);
        }

        [Fact]
        public void Return_Different_Cards_On_Every_Draw()
        {
            var card1 = _deck.Top;
            _deck.DrawTopCard(card1);

            var card2 = _deck.Top;
            _deck.DrawTopCard(card2);

            Check.That(card1).Not.IsEqualTo(card2);
            Check.That(_deck.DrawnCardsCount).IsEqualTo(2);
        }

        [Fact]
        public void Provide_The_Top_Card()
        {
            var staticDeck = new Deck(
                new[]{
                    Two.Of(Heart),
                    Three.Of(Spade),
                    Four.Of(Diamond)
            });

            Check.That(staticDeck.Top).Is(Two.Of(Heart));
        }

        [Fact]
        public void Provide_The_Top_Three_Cards()
        {
            var staticDeck = new Deck(
                new[]{
                    Two.Of(Heart),
                    Three.Of(Spade),
                    Four.Of(Diamond)
                });

            var top3Cards = staticDeck.Tops(3);

            Check.That(top3Cards).ContainsExactly(
                Two.Of(Heart),
                Three.Of(Spade),
                Four.Of(Diamond));
        }

        [Fact]
        public void Draw_a_specific_top_card()
        {
            var staticDeck = new Deck(
                new[]{
                    Two.Of(Heart),
                    Three.Of(Spade),
                    Four.Of(Diamond)
                });

            staticDeck.DrawTopCard(Two.Of(Heart));

            Check.That(staticDeck.DrawnCardsCount)
                .IsEqualTo(1);
            Check.That(staticDeck.IsDrawn(Two.Of(Heart)))
                .IsTrue();
        }

        [Fact]
        public void Not_Draw_a_specific_card_if_not_on_top()
        {
            var staticDeck = new Deck(
                new[]{
                    Two.Of(Heart),
                    Three.Of(Spade),
                    Four.Of(Diamond)
                });

            staticDeck.DrawTopCard(Three.Of(Spade));

            Check.That(staticDeck.DrawnCardsCount)
                .IsEqualTo(0);
            Check.That(staticDeck.IsDrawn(Two.Of(Heart)))
                .IsFalse();
        }

        [Fact]
        public void Not_Draw_a_specific_card_if_deck_is_empty()
        {
            var staticDeck = new Deck(
                new[]
                {
                    Three.Of(Spade)
                }
            );

            staticDeck.DrawTopCard(Three.Of(Spade));
            staticDeck.DrawTopCard(Four.Of(Spade));

            Check.That(staticDeck.DrawnCardsCount)
                .IsEqualTo(1);
            Check.That(staticDeck.IsDrawn(Three.Of(Spade)))
                .IsTrue();
            Check.That(staticDeck.IsDrawn(Four.Of(Spade)))
                .IsFalse();
        }

        [Fact]
        public void Display_all_cards_and_a_pointer_to_the_top_card()
        {
            var staticDeck = new Deck(
                new[] {
                    Three.Of(Spade),
                    Three.Of(Heart),
                    Ten.Of(Diamond),
                    Queen.Of(Heart),
                    King.Of(Club),
                }
            );

            staticDeck.DrawTopCard(staticDeck.Top);
            staticDeck.DrawTopCard(staticDeck.Top);
            staticDeck.DrawTopCard(staticDeck.Top);

            Check.That(staticDeck.ToString()).IsEqualTo(
                "3♠ 3♥ 10♦ [Q♥] K♣" 
                );
        }
    }
}