﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;


namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class ThreeOfAKindShould
    {
        [Fact]
        public void Beat_A_ThreeOfAKind_With_A_Smaller_Rank()
        {
            Check.That(This<ThreeOfAKind>("3♣ 3♥ 3♦ 5♠ J♦"))
                .Beats(This<ThreeOfAKind>("2♣ 2♥ 2♦ 3♠ 4♦"));
        }

        [Fact]
        public void Beat_A_ThreeOfAKind_With_The_Same_Rank_But_A_Bigger_Kicker()
        {
            Check.That(This<ThreeOfAKind>("3♣ 3♥ 3♦ 5♠ J♦"))
                .Beats(This<ThreeOfAKind>("3♣ 3♥ 3♦ 2♠ 4♦"));
        }

        [Fact]
        public void Beat_The_Biggest_HighCard()
        {
            Check.That(This<ThreeOfAKind>("2♣ 2♥ 2♦ 3♠ 4♦"))
                .Beats(This<HighCard>("A♦ Q♠ K♣ J♣ 9♥"));
        }

        [Fact]
        public void Beat_The_Biggest_Pair()
        {
            Check.That(This<ThreeOfAKind>("2♣ 2♥ 2♦ 3♠ 4♦"))
                .Beats(This<Pair>("A♦ Q♠ K♣ J♣ A♥"));
        }

        [Fact]
        public void Beat_The_Biggest_TwoPairs()
        {
            Check.That(This<ThreeOfAKind>("2♣ 2♥ 2♦ 3♠ 4♦"))
                .Beats(This<TwoPairs>("A♦ K♠ K♣ Q♣ A♥"));
        }

        [Fact]
        public void Not_Beat_Another_ThreeOfAKind_With_The_Same_Strength()
        {
            Check.That(This<ThreeOfAKind>("3♣ 3♥ 3♦ 5♠ J♦"))
                .Not.Beats(This<ThreeOfAKind>("3♠ 3♦ 3♥ 5♦ J♣"));
        }
    }
}