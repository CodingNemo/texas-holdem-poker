﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class FlushShould
    {
        [Fact]
        public void Beat_Another_Flush_With_A_Smaller_HighCard()
        {
            Check.That(This<Flush>("2♦ J♦ Q♦ T♦ 4♦"))
                .Beats(This<Flush>("2♦ J♦ 9♦ T♦ 4♦"));
        }

        [Fact]
        public void Beat_The_Biggest_HighCard()
        {
            Check.That(This<Flush>("2♦ 3♦ 4♦ 5♦ 7♦"))
                .Beats(This<HighCard>("A♠ K♦ Q♥ J♦ 9♣"));
        }

        [Fact]
        public void Beat_The_Biggest_Pair()
        {
            Check.That(This<Flush>("2♦ 3♦ 4♦ 5♦ 7♦"))
                .Beats(This<Pair>("A♠ A♦ K♥ J♦ Q♣"));
        }

        [Fact]
        public void Beat_The_Biggest_Straight()
        {
            Check.That(This<Flush>("2♦ 3♦ 4♦ 5♦ 7♦"))
                .Beats(This<Straight>("A♠ K♦ Q♥ J♦ T♣"));
        }

        [Fact]
        public void Beat_The_Biggest_ThreeOfAKind()
        {
            Check.That(This<Flush>("2♦ 3♦ 4♦ 5♦ 7♦"))
                .Beats(This<ThreeOfAKind>("A♠ A♦ A♥ K♦ Q♣"));
        }

        [Fact]
        public void Beat_The_Biggest_TwoPairs()
        {
            Check.That(This<Flush>("2♦ 3♦ 4♦ 5♦ 7♦"))
                .Beats(This<TwoPairs>("A♠ A♦ K♥ K♦ Q♣"));
        }

        [Fact]
        public void Not_Beat_Another_Flush_With_The_Same_Strength()
        {
            Check.That(This<Flush>("2♦ J♦ Q♦ T♦ 4♦"))
                .Not.Beats(This<Flush>("2♦ J♦ Q♦ T♦ 4♦"));
        }
    }
}