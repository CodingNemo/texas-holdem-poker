﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class HighCardShould
    {
        [Fact]
        public void Beat_A_Quite_Similar_HighCard_Except_For_The_Smallest_Card()
        {
            Check.That(This<HighCard>("A♣ K♥ 3♣ 4♠ T♣"))
                .Beats(This<HighCard>("A♣ K♥ 2♣ 4♠ T♣"));
        }

        [Fact]
        public void Beat_A_Smaller_HighCard()
        {
            Check.That(This<HighCard>("A♣ K♥ 2♣ 3♠ T♣"))
                .Beats(This<HighCard>("Q♦ T♥ 6♠ 7♥ J♣"));
        }

        [Fact]
        public void Does_Not_Beat_The_Same_Hand()
        {
            Check.That(This<HighCard>("A♣ K♥ 2♣ 3♠ T♣"))
                .Not.Beats(This<HighCard>("A♣ K♥ 2♣ 3♠ T♣"));
        }
    }
}