﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class StraightFlushShould
    {
        [Fact]
        public void Beat_Another_StraightFlush_With_A_Smaller_HighCard_Rank()
        {
            Check.That(This<StraightFlush>("6♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"));
        }

        [Fact]
        public void Beat_The_Biggest_Flush()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<Flush>("A♠ K♠ Q♠ J♠ 9♠"));
        }

        [Fact]
        public void Beat_The_Biggest_FourOfAKind()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<FourOfAKind>("A♠ A♣ A♥ A♦ K♠"));
        }

        [Fact]
        public void Beat_The_Biggest_HighCard()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<HighCard>("A♠ K♦ Q♥ J♦ 9♣"));
        }

        [Fact]
        public void Beat_The_Biggest_Pair()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<Pair>("A♠ A♦ K♥ J♦ Q♣"));
        }

        [Fact]
        public void Beat_The_Biggest_Straight()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<Straight>("A♠ K♦ Q♥ J♦ T♣"));
        }

        [Fact]
        public void Beat_The_Biggest_ThreeOfAKind()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<ThreeOfAKind>("A♠ A♦ A♥ K♦ Q♣"));
        }

        [Fact]
        public void Beat_The_Biggest_TwoPairs()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Beats(This<TwoPairs>("A♠ A♦ K♥ K♦ Q♣"));
        }

        [Fact]
        public void Not_Beat_Another_StraightFlush_With_The_Same_Strength()
        {
            Check.That(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"))
                .Not.Beats(This<StraightFlush>("A♦ 2♦ 3♦ 4♦ 5♦"));
        }
    }
}