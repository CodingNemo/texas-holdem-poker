﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Hands;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    internal class HandValueFactory
    {
        private const int SAMPLE_SIZE = 750;

        private static readonly StringToHandParser HandParser =
            new StringToHandParser(new StringToCardParser(new Deck()));

        private static readonly HandEvaluator HandEvaluator = new HandEvaluator();

        private static readonly Random Random = new Random();

        internal static T This<T>(string hand)
            where T : HandValue
        {
            var handValue =
                HandEvaluator.Evaluate(
                    HandParser.Parse(hand));

            if (!(handValue is T evaluatedHand))
            {
                throw new WrongHandValue($"Expected {typeof(Straight).Name}. A♣tual {handValue.GetType().Name}");
            }

            return evaluatedHand;
        }

        public static List<T> Some<T>(int sample = SAMPLE_SIZE)
            where T : HandValue
        {
            IEnumerable<string> lines;

            var directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileName = Path.Combine(directory, "Hands", "Samples", $"{typeof(T).Name}.txt");

            if (sample > 0)
            {
                lines = SampleOfLines(fileName, sample);
            }
            else
            {
                lines = AllLines(fileName);
            }

            return lines.Select(This<T>).ToList();
        }


        private static IEnumerable<string> SampleOfLines(string fileName, int sample)
        {
            using (var sr = new StreamReader(fileName))
            {
                var lastPos = sr.BaseStream.Seek(0, SeekOrigin.End);

                for (var i = 0; i < sample; ++i)
                {
                    var pct = Random.NextDouble(); // [0.0, 1.0)
                    var randomPos = (long) (pct * lastPos);

                    sr.BaseStream.Seek(randomPos, SeekOrigin.Begin);

                    var line = sr.ReadLine();
                    line = sr.ReadLine(); // this will be a full line
                    sr.DiscardBufferedData(); // magic

                    if (string.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    yield return line;
                }
            }
        }

        private static IEnumerable<string> AllLines(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    yield return reader.ReadLine();
                }
            }
        }

        public class WrongHandValue : Exception
        {
            public WrongHandValue(string message) : base(message)
            {
            }
        }
    }
}