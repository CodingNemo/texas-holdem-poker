﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class FourOfAKindShould
    {
        [Fact]
        public void Beat_Another_FourOfAKind_With_A_Smaller_Rank()
        {
            Check.That(This<FourOfAKind>("4♦ 4♣ 4♥ 4♠ 3♠"))
                .Beats(This<FourOfAKind>("2♦ 2♣ 2♥ 2♠ 3♠"));
        }

        [Fact]
        public void Beat_Another_FourOfAKind_With_The_Same_Rank_But_A_Smaller_Kicker()
        {
            Check.That(This<FourOfAKind>("4♦ 4♣ 4♥ 4♠ 3♠"))
                .Beats(This<FourOfAKind>("4♦ 4♣ 4♥ 4♠ 2♦"));
        }

        [Fact]
        public void Beat_The_Biggest_Flush()
        {
            Check.That(This<FourOfAKind>("2♦ 2♣ 2♥ 2♠ 3♠"))
                .Beats(This<Flush>("A♠ K♠ Q♠ J♠ 9♠"));
        }

        [Fact]
        public void Beat_The_Biggest_HighCard()
        {
            Check.That(This<FourOfAKind>("2♦ 2♣ 2♥ 2♠ 3♠"))
                .Beats(This<HighCard>("A♠ K♦ Q♥ J♦ 9♣"));
        }


        [Fact]
        public void Beat_The_Biggest_Pair()
        {
            Check.That(This<FourOfAKind>("2♦ 2♣ 2♥ 2♠ 3♠"))
                .Beats(This<Pair>("A♠ A♦ K♥ J♦ Q♣"));
        }

        [Fact]
        public void Beat_The_Biggest_Straight()
        {
            Check.That(This<FourOfAKind>("2♦ 2♣ 2♥ 2♠ 3♠"))
                .Beats(This<Straight>("A♠ K♦ Q♥ J♦ T♣"));
        }

        [Fact]
        public void Beat_The_Biggest_ThreeOfAKind()
        {
            Check.That(This<FourOfAKind>("2♦ 2♣ 2♥ 2♠ 3♠"))
                .Beats(This<ThreeOfAKind>("A♠ A♦ A♥ K♦ Q♣"));
        }


        [Fact]
        public void Beat_The_Biggest_TwoPairs()
        {
            Check.That(This<FourOfAKind>("2♦ 2♣ 2♥ 2♠ 3♠"))
                .Beats(This<TwoPairs>("A♣ A♦ K♥ K♦ Q♣"));
        }

        [Fact]
        public void Not_Beat_Another_FourOfAKind_With_The_Same_Strength()
        {
            Check.That(This<FourOfAKind>("4♦ 4♣ 4♥ 4♠ 3♠"))
                .Not.Beats(This<FourOfAKind>("4♦ 4♣ 4♥ 4♠ 3♦"));
        }
    }
}