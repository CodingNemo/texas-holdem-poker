﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands
{
    public class HandValueSamplesComparisonTests
    {
        [Fact]
        public void Flush_Beats_Every_HighCards() =>
            Check.That(Some<Flush>()).Beats(Some<HighCard>());

        [Fact]
        public void Flush_Beats_Every_Pairs() =>
            Check.That(Some<Flush>()).Beats(Some<Pair>());

        [Fact]
        public void Flush_Beats_Every_Straights() =>
            Check.That(Some<Flush>()).Beats(Some<Straight>());

        [Fact]
        public void Flush_Beats_Every_ThreeOfAKinds() =>
            Check.That(Some<Flush>()).Beats(Some<ThreeOfAKind>());

        [Fact]
        public void Flush_Beats_Every_TwoPairs() =>
            Check.That(Some<Flush>()).Beats(Some<TwoPairs>());

        [Fact]
        public void FourOfAKind_Beats_Every_Flushes() =>
            Check.That(Some<FourOfAKind>()).Beats(Some<Flush>());

        [Fact]
        public void FourOfAKind_Beats_Every_FullHouses() =>
            Check.That(Some<FourOfAKind>()).Beats(Some<FullHouse>());

        [Fact]
        public void FourOfAKind_Beats_Every_HighCards() =>
            Check.That(Some<FourOfAKind>()).Beats(Some<HighCard>());

        [Fact]
        public void FourOfAKind_Beats_Every_Pairs() =>
            Check.That(Some<FourOfAKind>()).Beats(Some<Pair>());

        [Fact]
        public void FourOfAKind_Beats_Every_Straights() =>
            Check.That(Some<FourOfAKind>()).Beats(Some<Straight>());

        [Fact]
        public void FourOfAKind_Beats_Every_ThreeOfAKinds() =>
            Check.That(Some<FourOfAKind>()).Beats(Some<ThreeOfAKind>());

        [Fact]
        public void FourOfAKind_Beats_Every_TwoPairs() =>
            Check.That(Some<FourOfAKind>()).Beats(Some<TwoPairs>());

        [Fact]
        public void FullHouse_Beats_Every_Flushes() =>
            Check.That(Some<FullHouse>()).Beats(Some<Flush>());

        [Fact]
        public void FullHouse_Beats_Every_HighCards() =>
            Check.That(Some<FullHouse>()).Beats(Some<HighCard>());

        [Fact]
        public void FullHouse_Beats_Every_Pairs() =>
            Check.That(Some<FullHouse>()).Beats(Some<Pair>());

        [Fact]
        public void FullHouse_Beats_Every_Straights() =>
            Check.That(Some<FullHouse>()).Beats(Some<Straight>());

        [Fact]
        public void FullHouse_Beats_Every_ThreeOfAKinds() =>
            Check.That(Some<FullHouse>()).Beats(Some<ThreeOfAKind>());

        [Fact]
        public void FullHouse_Beats_Every_TwoPairs() =>
            Check.That(Some<FullHouse>()).Beats(Some<TwoPairs>());

        [Fact]
        public void Pair_Beats_Every_HighCards() =>
            Check.That(Some<Pair>()).Beats(Some<HighCard>());

        [Fact]
        public void Straight_Beats_Every_HighCards() =>
            Check.That(Some<Straight>()).Beats(Some<HighCard>());

        [Fact]
        public void Straight_Beats_Every_Pairs() =>
            Check.That(Some<Straight>()).Beats(Some<Pair>());

        [Fact]
        public void Straight_Beats_Every_ThreeOfAKinds() =>
            Check.That(Some<Straight>()).Beats(Some<ThreeOfAKind>());

        [Fact]
        public void Straight_Beats_Every_TwoPairs() =>
            Check.That(Some<Straight>()).Beats(Some<TwoPairs>());

        [Fact]
        public void StraightFlushes_Beats_Every_Flushes() =>
            Check.That(Some<StraightFlush>()).Beats(Some<Flush>());

        [Fact]
        public void StraightFlushes_Beats_Every_FourOfKinds() =>
            Check.That(Some<StraightFlush>()).Beats(Some<FourOfAKind>());

        [Fact]
        public void StraightFlushes_Beats_Every_FullHouses() =>
            Check.That(Some<StraightFlush>()).Beats(Some<FullHouse>());

        [Fact]
        public void StraightFlushes_Beats_Every_HighCards() =>
            Check.That(Some<StraightFlush>()).Beats(Some<HighCard>());

        [Fact]
        public void StraightFlushes_Beats_Every_Pairs() =>
            Check.That(Some<StraightFlush>()).Beats(Some<Pair>());

        [Fact]
        public void StraightFlushes_Beats_Every_Straights() =>
            Check.That(Some<StraightFlush>()).Beats(Some<Straight>());

        [Fact]
        public void StraightFlushes_Beats_Every_ThreeOfAKinds() =>
            Check.That(Some<StraightFlush>()).Beats(Some<ThreeOfAKind>());

        [Fact]
        public void StraightFlushes_Beats_Every_TwoPairs() =>
            Check.That(Some<StraightFlush>()).Beats(Some<TwoPairs>());

        [Fact]
        public void ThreeOfAKind_Beats_Every_HighCards() =>
            Check.That(Some<ThreeOfAKind>()).Beats(Some<HighCard>());

        [Fact]
        public void ThreeOfAKind_Beats_Every_Pairs() =>
            Check.That(Some<ThreeOfAKind>()).Beats(Some<Pair>());

        [Fact]
        public void ThreeOfAKind_Beats_Every_TwoPairs() =>
            Check.That(Some<ThreeOfAKind>()).Beats(Some<TwoPairs>());

        [Fact]
        public void TwoPair_Beats_Every_HighCards() =>
            Check.That(Some<TwoPairs>()).Beats(Some<HighCard>());

        [Fact]
        public void TwoPair_Beats_Every_Pairs() =>
            Check.That(Some<TwoPairs>()).Beats(Some<Pair>());
    }
}