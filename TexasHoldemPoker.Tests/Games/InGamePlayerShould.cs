﻿using NFluent;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Players;
using Xunit;

using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Tests.Games
{
    public class InGamePlayerShould
    {
        private readonly InGamePlayer player = new InGamePlayer(PlayerId.New("John"), 1000);

        [Fact]
        public void Have_Less_Chips_When_Paying()
        {
            player.Pay(100);

            Check.That(player.ChipsValue).IsEqualTo(900);
        }

        [Fact]
        public void Have_Moved_This_Round_When_Folded()
        {
            player.Fold(Flop);
            CheckHasMoved(Flop);
        }

        [Fact]
        public void Have_Moved_This_Round_When_Called()
        {
            player.Call(Flop);
            CheckHasMoved(Flop);
        }

        [Fact]
        public void Have_Moved_This_Round_When_Bet()
        {
            player.Bet(Flop);
            CheckHasMoved(Flop);
        }

        [Fact]
        public void Have_Moved_This_Round_When_Raised()
        {
            player.Raise(Flop);
            CheckHasMoved(Flop);
        }

        [Fact]
        public void Have_Moved_This_Round_When_AllIn()
        {
            player.AllIn(Flop);
            CheckHasMoved(Flop);
        }

        [Fact]
        public void Have_Not_Moved_This_Round()
        {
            player.Bet(Preflop);
            Check.That(player.HasMoved(Flop)).IsFalse();
        }

        private void CheckHasMoved(BettingRoundName round)
        {
            Check.That(player.HasMoved(round)).IsTrue();
        }
    }
}
