﻿using System.Collections.Generic;
using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;
using Xunit.Abstractions;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class PreflopShould : Feature
    {
        public readonly GameSamples game = new GameSamples();

        public PreflopShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Fact]
        public void Start_When_A_Game_Has_Been_Started() =>
            Given.A<Game>()
                .When(new StartGame(game.Id, game.DeckCards, game.Blinds, game.Players.DealerId, game.Players.All))
                .Then(
                    CheckNewHistory.Skip(1)
                        .Equals(
                            new BettingRoundStarted(game.Id, Preflop, game.Players.BigBlindId)
                        )
                );

        [Fact]
        public void Deal_Two_Cards_Per_Player_When_Starting() =>
            Given.A<Game>()
                .When(new StartGame(game.Id, game.DeckCards, game.Blinds, game.Players.DealerId, game.Players.All))
                .Then(
                    CheckNewHistory.Skip(2)
                        .Equals(
                            new CardDealtToPlayer(game.Id, game.Players.SmallBlindId, game.Hands.SmallBlind.First),
                            new CardDealtToPlayer(game.Id, game.Players.BigBlindId, game.Hands.BigBlind.First),
                            new CardDealtToPlayer(game.Id, game.Players.UnderTheGunId, game.Hands.UnderTheGun.First),
                            new CardDealtToPlayer(game.Id, game.Players.DealerId, game.Hands.Dealer.First),
                            new CardDealtToPlayer(game.Id, game.Players.SmallBlindId, game.Hands.SmallBlind.Last),
                            new CardDealtToPlayer(game.Id, game.Players.BigBlindId, game.Hands.BigBlind.Last),
                            new CardDealtToPlayer(game.Id, game.Players.UnderTheGunId, game.Hands.UnderTheGun.Last),
                            new CardDealtToPlayer(game.Id, game.Players.DealerId, game.Hands.Dealer.Last)
                        )
                );

        [Fact]
        public void Make_The_Players_Post_The_Game_Blinds_When_Started() =>
            Given.A<Game>()
                .When(new StartGame(game.Id, game.DeckCards, game.Blinds, game.Players.DealerId, game.Players.All))
                .Then(
                    CheckNewHistory.Skip(10)
                        .Equals(
                            new BlindPosted(game.Id, game.Players.SmallBlindId, game.Blinds.Small),
                            new BlindPosted(game.Id, game.Players.BigBlindId, game.Blinds.Big)
                        )
                );

        [Fact]
        public void Start_UTG_Player_Turn_When_Started() =>
            Given.A<Game>()
                .When(new StartGame(game.Id, game.DeckCards, game.Blinds, game.Players.DealerId, game.Players.All))
                .Then(
                    CheckNewHistory.EndsWith(
                        new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId))
                );

        [Fact]
        public void Fire_A_PlayerBet_Event_When_UTG_Player_Calls() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .When(new PlayerCalls(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerCalled(game.Id, game.Players.UnderTheGunId),
                        new PlayerTurnStarted(game.Id, game.Players.DealerId)));

        [Fact]
        public void Fire_A_PlayerFolded_Event_When_Dealer_Player_Folds() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .And(game.Preflop.UTGCalled)
                .When(new PlayerFolds(game.Id, game.Players.DealerId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerFolded(game.Id, game.Players.DealerId),
                        new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)));

        [Fact]
        public void Fire_A_PlayerCalled_Event_When_SmallBlind_Player_Calls() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .And(game.Preflop.UTGCalled)
                .And(game.Preflop.DealerFolded)
                .When(new PlayerCalls(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerCalled(game.Id, game.Players.SmallBlindId),
                        new PlayerTurnStarted(game.Id, game.Players.BigBlindId)));

        [Fact]
        public void Fire_A_PlayerChecked_Event_When_BigBlind_Player_Checks() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .And(game.Preflop.UTGCalled)
                .And(game.Preflop.DealerFolded)
                .And(game.Preflop.SBCalled)
                .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                .Then(
                    CheckNewHistory.StartsWith(
                            new PlayerChecked(game.Id, game.Players.BigBlindId)));

        [Fact]
        public void End_When_All_Players_Have_Finished() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Started)
                .And(game.Preflop.UTGCalled)
                .And(game.Preflop.DealerFolded)
                .And(game.Preflop.SBCalled)
                .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                .Then(
                    CheckNewHistory.Skip(1)
                        .Equals(
                            new BettingRoundIsOver(game.Id, Preflop,
                                new Dictionary<PlayerId, int>()
                                {
                                    [game.Players.UnderTheGunId] = game.Blinds.Big,
                                    [game.Players.SmallBlindId] = game.Blinds.Big,
                                    [game.Players.BigBlindId] = game.Blinds.Big,
                                })));
    }
}