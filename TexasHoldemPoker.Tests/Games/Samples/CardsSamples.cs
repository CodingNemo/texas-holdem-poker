﻿using TexasHoldemPoker.Cards;

using static TexasHoldemPoker.Cards.Rank;
using static TexasHoldemPoker.Cards.Suit;

namespace TexasHoldemPoker.Tests.Games.Samples
{
    public class CardsSamples
    {
        public readonly Card BurntBeforeFlop = Ace.Of(Spade);
        public readonly Card[] Flop = { Three.Of(Club), Queen.Of(Heart), Four.Of(Diamond) };

        public readonly Card BurntBeforeTurn = Nine.Of(Heart);
        public readonly Card Turn = King.Of(Diamond);

        public readonly Card BurntBeforeRiver = Queen.Of(Diamond);
        public readonly Card River = Jack.Of(Spade);
    }
}
