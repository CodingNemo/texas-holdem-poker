using NFluent;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;

namespace TexasHoldemPoker.Tests.Games.BettingRounds
{
    public class PreFlopRoundShould
    {
        private readonly PlayersSamples players = new PlayersSamples();
        private readonly TestablePreFlopRound bettingRound;

        private class TestablePreFlopRound : PreflopRound
        {
            public TestablePreFlopRound(PlayerId dealerId, IEnumeratePlayerIds players)
            : base(GameId.New(TableId.New()), new Deck(), new Blinds(50, 100), new Bets(),
                  dealerId, players, _ => { })
            { }

            public BettingRoundName Name => CurrentBettingRoundName;

            public bool TestableIsOver()
            {
                return IsOver();
            }
        }

        public PreFlopRoundShould()
        {
            bettingRound = new TestablePreFlopRound(
                players.Dealer.Id,
                players.AllIds
            );
        }

        [Fact]
        public void Be_Over_When_Every_body_Calls_And_The_BB_Checks()
        {
            players.UnderTheGun.Call(bettingRound.Name);
            players.Dealer.Call(bettingRound.Name);
            players.SmallBlind.Call(bettingRound.Name);
            players.BigBlind.Check(bettingRound.Name);

            Check.That(bettingRound.TestableIsOver()).IsTrue();
        }

        [Fact]
        public void Ignore_Folded_Players()
        {
            players.UnderTheGun.Fold(bettingRound.Name);
            players.Dealer.Call(bettingRound.Name);
            players.SmallBlind.Call(bettingRound.Name);
            players.BigBlind.Check(bettingRound.Name);

            Check.That(bettingRound.TestableIsOver()).IsTrue();
        }
    }
}