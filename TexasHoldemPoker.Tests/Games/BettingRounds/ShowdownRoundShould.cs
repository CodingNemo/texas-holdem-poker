﻿using NFluent;
using System;
using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using TexasHoldemPoker.Framework;
using Xunit;


namespace TexasHoldemPoker.Tests.Games.BettingRounds
{
    public class ShowdownRoundShould
    {
        private static readonly GameId gameId = GameId.New(TableId.New());
        private StringToCardParser CardParser => new StringToCardParser(new Deck());
        private StringToHandParser HandParser => new StringToHandParser(CardParser);

        private readonly PlayersSamples players = new PlayersSamples();

        private readonly InGameHands hands = new InGameHands();
        private readonly Pots pots = new Pots();
        private readonly Board board = new Board();

        private readonly TestableShowdownRound showdownRound;
        private class TestableShowdownRound : ShowdownRound
        {
            public TestableShowdownRound(Board board, InGameHands hands, Pots pots, PlayerId dealerId, IEnumeratePlayerIds players, Action<DomainEvent> fire)
                : base(gameId, new Deck(), new Bets(),
                      board, hands, pots, dealerId, players, fire)
            { }


            public IEnumerable<PlayerId> TestableWinners(Pot pot)
            {
                return Winners(pot);
            }
        }

        private readonly List<DomainEvent> history = new List<DomainEvent>();

        public ShowdownRoundShould()
        {
            showdownRound = new TestableShowdownRound(board, hands, pots,
                players.DealerId, players.AllIds, de => history.Add(de));
        }

        [Fact]
        public void Find_The_Winning_Player_Contributing_To_A_Pot()
        {
            var pot = new Pot();

            Deal(players.DealerId, "A♥ A♦");
            pot.Contribute(players.DealerId, 1_000, false);

            Deal(players.SmallBlindId, "2♣ 5♠");
            pot.Contribute(players.SmallBlindId, 1_000, false);

            BoardIs("A♠ A♣ K♣ T♥ 2♦");

            var winners = showdownRound.TestableWinners(pot);

            Check.That(winners).ContainsExactly(players.DealerId);
        }

        [Fact]
        public void Find_The_Winning_Players_Contributing_To_A_Pot()
        {
            var pot = new Pot();

            Deal(players.DealerId, "A♥ 2♦");
            pot.Contribute(players.DealerId, 1_000, false);

            Deal(players.SmallBlindId, "A♣ 5♠");
            pot.Contribute(players.SmallBlindId, 1_000, false);

            BoardIs("A♠ A♦ K♣ T♥ 8♣");

            var winners = showdownRound.TestableWinners(pot);

            Check.That(winners)
                 .ContainsExactly(players.DealerId, players.SmallBlindId);
        }

        [Fact]
        public void Pay_Each_Winner_A_Fair_Share_Of_The_Pot()
        {
            var pot = pots.Current;

            Deal(players.DealerId, "A♥ 2♦");
            pot.Contribute(players.DealerId, 1_000, false);

            pot.Contribute(players.BigBlindId, 1_000, false);
            players.BigBlind.Fold(BettingRoundName.River);

            Deal(players.SmallBlindId, "A♣ 5♠");
            pot.Contribute(players.SmallBlindId, 1_000, false);

            BoardIs("A♠ A♦ K♣ T♥ 8♣");

            showdownRound.Continue();

            Check.That(history)
                .EndsWith(new DomainEvent[] {
                    new PlayerWon(gameId, players.DealerId, 1_500),
                    new PlayerWon(gameId, players.SmallBlindId, 1_500) });
        }


        [Fact]
        public void Find_A_Winner_Per_Pot()
        {
            Deal(players.DealerId, "A♥ K♦");
            pots.AddLastContribution(players.DealerId, 1_000);

            Deal(players.BigBlindId, "T♣ 4♦");
            pots.AddLastContribution(players.BigBlindId, 2_000);

            Deal(players.SmallBlindId, "8♠ 5♠");
            pots.AddContribution(players.SmallBlindId, 3_000);

            Deal(players.UnderTheGunId, "2♥ 4♥");
            pots.AddContribution(players.UnderTheGunId, 3_000);

            BoardIs("A♠ A♦ K♣ T♥ 8♣");

            showdownRound.Continue();

            Check.That(history)
                .EndsWith(new DomainEvent[] {
                new PlayerWon(gameId, players.DealerId, 4_000),
                new PlayerWon(gameId, players.BigBlindId, 3_000),
                new PlayerWon(gameId, players.SmallBlindId, 2_000) });
        }

        private void BoardIs(string boardAsString)
        {
            var cards = boardAsString.Split(" ")
                                     .Select(CardParser.Parse);
            cards.ForEach(board.Add);
        }

        private void Deal(PlayerId player, string handAsString)
        {
            var hand = HandParser.Parse(handAsString);
            hands[player].AddCard(hand.First).AddCard(hand.Last);
        }
    }
}
