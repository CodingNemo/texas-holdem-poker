﻿using System;
using NFluent;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;

namespace TexasHoldemPoker.Tests.Games.BettingRounds
{
    public class BettingRoundShould
    {
        private readonly PlayersSamples players = new PlayersSamples();

        private readonly SomeBettingRound bettingRound;
        private class SomeBettingRound : BettingRound
        {
            public SomeBettingRound(PlayerId dealerId, IEnumeratePlayerIds players)
                : base(GameId.New(TableId.New()), new Deck(), new Bets(), dealerId, players, _ => { })
            {
            }

            protected override void OnStart()
            {
                throw new NotImplementedException();
            }

            public override BettingRoundName NextBettingRoundName { get; }

            public override BettingRoundName CurrentBettingRoundName { get; }

            public BettingRoundName Name => CurrentBettingRoundName;

            public bool TestableIsOver()
            {
                return IsOver();
            }
        }

        public BettingRoundShould()
        {
            bettingRound = new SomeBettingRound(
                        players.Dealer.Id,
                        players.AllIds
                );
        }

        [Fact]
        public void Not_Be_Over_When_Some_Players_Have_Not_Played_Yet()
        {
            players.UnderTheGun.Check(bettingRound.Name);
            players.Dealer.Check(bettingRound.Name);
            players.SmallBlind.Check(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            // BBPlayer has not played yet
            Check.That(isOver).IsFalse();
        }


        [Fact]
        public void Be_Over_When_All_Players_Have_Checked()
        {
            players.UnderTheGun.Check(bettingRound.Name);
            players.Dealer.Check(bettingRound.Name);
            players.SmallBlind.Check(bettingRound.Name);
            players.BigBlind.Check(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            Check.That(isOver).IsTrue();
        }

        [Fact]
        public void Be_Over_When_A_Player_Bet_And_All_Others_Called()
        {
            players.UnderTheGun.Bet(bettingRound.Name);

            players.Dealer.Call(bettingRound.Name);
            players.SmallBlind.Call(bettingRound.Name);
            players.BigBlind.Call(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            Check.That(isOver).IsTrue();
        }


        [Fact]
        public void Be_Over_When_A_Player_Raises_And_All_Others_Called()
        {
            players.Dealer.Raise(bettingRound.Name);
            players.SmallBlind.Call(bettingRound.Name);
            players.BigBlind.Call(bettingRound.Name);
            players.UnderTheGun.Call(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            Check.That(isOver).IsTrue();
        }

        [Fact]
        public void Be_Over_When_A_Player_IsAllIn_And_All_Others_Called()
        {
            players.UnderTheGun.AllIn(bettingRound.Name);

            players.Dealer.Call(bettingRound.Name);
            players.SmallBlind.Call(bettingRound.Name);
            players.BigBlind.Call(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            Check.That(isOver).IsTrue();
        }

        [Fact]
        public void Not_Be_Over_When_A_Player_Raises_After_A_Players_Bet()
        {
            players.UnderTheGun.Bet(bettingRound.Name);
            players.Dealer.Raise(bettingRound.Name);
            players.SmallBlind.Call(bettingRound.Name);
            players.BigBlind.Call(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            Check.That(isOver).IsFalse();
        }


        [Fact]
        public void Ignore_Folded_Players_When_A_Player_Bet()
        {
            players.UnderTheGun.Bet(bettingRound.Name);

            players.Dealer.Fold(bettingRound.Name);
            players.SmallBlind.Call(bettingRound.Name);
            players.BigBlind.Call(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            Check.That(isOver).IsTrue();
        }

        [Fact]
        public void Ignore_Folded_Players_When_A_Players_Check()
        {
            players.UnderTheGun.Check(bettingRound.Name);

            players.Dealer.Check(bettingRound.Name);
            players.SmallBlind.Check(bettingRound.Name);
            players.BigBlind.Fold(bettingRound.Name);

            var isOver = bettingRound.TestableIsOver();

            Check.That(isOver).IsTrue();
        }
    }
}
