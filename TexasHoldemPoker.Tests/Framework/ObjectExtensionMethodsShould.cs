﻿using NFluent;
using System.Collections.Generic;
using TexasHoldemPoker.Framework;
using Value;
using Xunit;

namespace TexasHoldemPoker.Tests.Framework
{
    public class ObjectExtensionMethodsShould
    {
        public class SomethingId : ValueType<SomethingId>
        {
            private readonly int value;

            public SomethingId(int value)
            {
                this.value = value;
            }

            protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
            {
                return new object[] { value };
            }

            public override string ToString()
            {
                return $"n°{value}";
            }
        }

        class Something : Aggregate
        {
            public Something(History history, ILogEvents log, StateProjection state)
                : base(history, log, state)
            {
            }
        }

        class CreateSomething : Command
        {
            public object AggregateId => SomethingId;
            public SomethingId SomethingId { get; }

            public CreateSomething(SomethingId somethingId)
            {
                SomethingId = somethingId;
            }
        }

        class SomeoneId
        {
            public SomeoneId(string name)
            {
                Name = name;
            }

            public string Name { get; }

            public override string ToString()
            {
                return Name;
            }
        }

        class SomeoneDidAStuff : Command
        {
            public object AggregateId { get; }
            public SomeoneId SomeoneId { get; }

            public SomeoneDidAStuff(SomethingId id, SomeoneId someoneId)
            {
                AggregateId = id;
                SomeoneId = someoneId;
            }
        }

        class ChangeSomething : Command
        {
            public object AggregateId => SomethingId;
            public SomethingId SomethingId { get; }
            public string Changed { get; }
            public ChangeSomething(SomethingId somethingId, string changed)
            {
                SomethingId = somethingId;
                Changed = changed;
            }
        }

        class ChangeAnArrayOnSomething : Command
        {
            public object AggregateId => SomethingId;
            public SomethingId SomethingId { get; }
            public List<int> NewArray { get; }
            public ChangeAnArrayOnSomething(SomethingId somethingId, List<int> newArray)
            {
                SomethingId = somethingId;
                NewArray = newArray;
            }
        }

        class ChangeADictionnaryOnSomething : Command
        {

            public ChangeADictionnaryOnSomething(SomethingId somethingId, Dictionary<int, char> newDict)
            {
                SomethingId = somethingId;
                NewDict = newDict;
            }

            public SomethingId SomethingId { get; }
            public Dictionary<int, char> NewDict { get; }

            public object AggregateId => SomethingId;
        }

        class SomestuffCreated : DomainEvent<SomestuffCreated>
        {
            public override object AggregateId { get; }

            public string Somestuff { get; }

            public SomestuffCreated(SomethingId id, string somestuff)
            {
                AggregateId = id;
                Somestuff = somestuff;
            }
        }

        class SomeStuffAdded : DomainEvent<SomestuffCreated>
        {
            public override object AggregateId { get; }

            public int StuffValue { get; }

            public SomeStuffAdded(SomethingId id, int stuffValue)
            {
                AggregateId = id;
                StuffValue = stuffValue;
            }
        }

        [Fact]
        public void Generate_a_sentence_from_an_object_type_name()
        {
            var command = new CreateSomething(new SomethingId(1));

            var sentence = command.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Create Something(Id=n°1)");
        }

        [Fact]
        public void Generate_a_sentence_from_an_object_with_additional_properties()
        {
            var command = new ChangeSomething(new SomethingId(5), "Stuff");

            var sentence = command.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Change Something(Id=n°5) with Changed=Stuff");
        }

        [Fact]
        public void Generate_a_sentence_from_a_command_with_an_id_not_related_to_Aggregate()
        {
            var command = new SomeoneDidAStuff(new SomethingId(1_000), new SomeoneId("John"));

            var sentence = command.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Someone(Id=John) did a stuff");
        }

        [Fact]
        public void Generate_a_sentence_from_a_command_with_an_enumerable_property()
        {
            var command = new ChangeAnArrayOnSomething(new SomethingId(10), new List<int> { 1, 10, 100, 1000 });

            var sentence = command.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Change an array on Something(Id=n°10) with NewArray=[1,10,100,1000]");
        }

        [Fact]
        public void Generate_a_sentence_from_a_command_with_an_dictionary_property()
        {
            var command = new ChangeADictionnaryOnSomething(new SomethingId(10), new Dictionary<int, char>
            {
                [1] = 'a',
                [5] = 'b',
                [-2] = 'c',
            });

            var sentence = command.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Change a dictionnary on Something(Id=n°10) with NewDict={[1]=a,[5]=b,[-2]=c}");
        }

        [Fact]
        public void Generate_a_sentence_from_a_command_with_a_null_property()
        {
            var command = new ChangeAnArrayOnSomething(new SomethingId(10), null);

            var sentence = command.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Change an array on Something(Id=n°10) with NewArray=<null>");
        }

        [Fact]
        public void Generate_a_sentence_from_a_command_including_AggregateId()
        {
            var command = new SomeoneDidAStuff(new SomethingId(1_000), new SomeoneId("John"));

            var sentence = command.ToReadableSentence(withAggregateId: true);

            Check.That(sentence).IsEqualTo("[n°1000] - Someone(Id=John) did a stuff");
        }

        [Fact]
        public void Generate_a_sentence_from_an_object_with_property_name_in_type_name()
        {
            var @event = new SomestuffCreated(new SomethingId(4), "PIKACHU");

            var sentence = @event.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Somestuff(PIKACHU) created");
        }

        [Fact]
        public void Generate_a_sentence_from_an_object_with_property_value()
        {
            var @event = new SomeStuffAdded(new SomethingId(4), 1_800);

            var sentence = @event.ToReadableSentence();

            Check.That(sentence).IsEqualTo("Some Stuff(Value=1800) added");
        }
    }
}
