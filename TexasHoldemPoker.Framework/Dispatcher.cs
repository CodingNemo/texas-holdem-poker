﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace TexasHoldemPoker.Framework
{
    public class Dispatcher
    {
        private readonly Storage storage;

        public Dispatcher(Storage storage)
        {
            this.storage = storage;
        }

        public void Dispatch<TAggregate>(Do<TAggregate> command)
            where TAggregate : Aggregate
        {
            storage.SearchById<TAggregate>(command.AggregateId)
                .OnFound(found => HandleCommand(command, found))
                .OnNotFound(() => throw new FailToDispatchCommand($"Could not find any aggregate with id {command.AggregateId}."));
        }

        public void Dispatch<TAggregate>(New<TAggregate> command)
            where TAggregate : Aggregate
        {
            TAggregate obj = storage.Create<TAggregate>();
            HandleCommand(command, obj);
        }

        private static void HandleCommand<TCommand, TAggregate>(
            TCommand command, TAggregate obj)
            where TCommand: Command
            where TAggregate: Aggregate
        {
            var commandType = command.GetType();

            var iHandleCommandType = typeof(IHandle<>).MakeGenericType(commandType);

            Type aggregateType = typeof(TAggregate);

            if (!iHandleCommandType.IsAssignableFrom(aggregateType))
            {
                throw new FailToDispatchCommand($"Aggregate {command.AggregateId} of type {aggregateType.Name} can not handle a command of type {commandType.Name}.");
            }

            InternalHandle(command, obj, commandType);
        }

        [DebuggerHidden]
        private static void InternalHandle<TCommand, TAggregate>(TCommand command, TAggregate obj, Type commandType)
            where TCommand : Command
            where TAggregate : Aggregate
        {
            var internalHandler =
                            InternalHandleCommandMethodInfo
                            .MakeGenericMethod(commandType);

            try
            {
                internalHandler.Invoke(null, new object[] { obj, command });
            }
            catch (TargetInvocationException tie)
            {
                throw tie.InnerException;
            }
        }

        private static readonly MethodInfo InternalHandleCommandMethodInfo =
            typeof(Dispatcher).GetMethod(nameof(InternalHandleCommand),
                    BindingFlags.Static | BindingFlags.NonPublic);

        private static void InternalHandleCommand<TCommand>(
            IHandle<TCommand> handler,  TCommand command)
            where TCommand : Command
        {
            handler.Handle(command);
        }
    }   
}
