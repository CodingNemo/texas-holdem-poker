﻿
namespace TexasHoldemPoker.Framework
{
    public interface Command
    {
        object AggregateId { get; }
    }

    public interface New<TAggregate> : Command
        where TAggregate : Aggregate
    {
    }

    public interface Do<TAggregate> : Command
        where TAggregate : Aggregate
    {
    }
}