﻿namespace TexasHoldemPoker.Cards
{
    public enum Suit : short
    {
        SuitLess = 0,
        Heart = 1,
        Diamond = 2,
        Spade = 3,
        Club = 4
    }
}