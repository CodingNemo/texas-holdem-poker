﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TexasHoldemPoker.Cards
{
    public class Deck
    {
        private readonly Card[] _cards;
        private readonly Dictionary<short, Card> _indexedCards = new Dictionary<short, Card>();

        private readonly Random shuffleRandom = new Random();

        public Deck()
            : this(BuildCards()) { }

        public Deck(Card[] cards)
        {
            _cards = cards;
            IndexCards(cards);
        }

        public static IEnumerable<Rank> Ranks { get; } =
            Enum.GetValues(typeof(Rank)).Cast<Rank>().Except(new[] { Rank.RankLess }).ToArray();

        public static IEnumerable<Suit> Suits { get; } =
            Enum.GetValues(typeof(Suit)).Cast<Suit>().Except(new[] { Suit.SuitLess }).ToArray();

        public int CardCount => _cards.Length;

        public bool Empty => DrawnCardsCount >= CardCount;

        public int DrawnCardsCount { get; private set; }

        public Card Top => _cards[DrawnCardsCount];

        public IEnumerable<Card> Cards => _cards;

        public IEnumerable<Card> Tops(int count)
        {
            for (var cardIndex = 0; cardIndex < count; cardIndex++)
            {
                yield return _cards[DrawnCardsCount + cardIndex];
            }
        }

        public void Shuffle()
        {
            ResetLastCardDrawn();

            for (var position = 0; position < CardCount; position++)
            {
                var newPosition = shuffleRandom.Next(CardCount);
                SwitchCardPosition(position, newPosition);
            }
        }

        private void DrawACard()
        {
            if (Empty)
            {
                throw new InvalidOperationException("Deck is empty");
            }

            DrawnCardsCount++;
        }

        public Card FindCard(Rank rank, Suit suit)
        {
            var key = Card.BuildIdFromRankAndSuit(rank, suit);
            return _indexedCards[key];
        }

        protected void ResetLastCardDrawn()
        {
            DrawnCardsCount = 0;
        }

        protected void SwitchCardPosition(int oldPosition, int newPosition)
        {
            var cardInOldPosition = _cards[oldPosition];

            _cards[oldPosition] = _cards[newPosition];
            _cards[newPosition] = cardInOldPosition;
        }

        private void IndexCards(Card[] cards)
        {
            foreach (var card in cards)
            {
                _indexedCards[card.Id] = card;
            }
        }

        private static Card[] BuildCards()
        {
            var cards = new Card[Ranks.Count() * Suits.Count()];

            short cardPosition = 0;

            foreach (var suit in Suits)
                foreach (var rank in Ranks)
                {
                    var card = rank.Of(suit);
                    cards[cardPosition++] = card;
                }

            return cards;
        }

        public override string ToString()
        {
            IEnumerable<string> GenerateString()
            {
                int cardCount = 1;
                foreach (var card in _cards)
                {

                    if (cardCount == DrawnCardsCount + 1)
                    {
                        yield return $"[{card.ToString()}]";
                    }
                    else
                    {
                        yield return card.ToString();
                    }
                    cardCount++;
                }
            }
            
            return string.Join(" ", GenerateString());
        }

        public void DrawTopCard(Card card)
        {
            if (Empty || !Top.Equals(card))
            {
                return;
            }

            DrawACard();
        }

        public bool IsDrawn(Card card)
        {
            var cardIndex = Array.IndexOf(_cards, card);
            return cardIndex != -1
            && DrawnCardsCount > cardIndex;
        }
    }
}