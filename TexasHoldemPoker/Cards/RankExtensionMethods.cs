﻿namespace TexasHoldemPoker.Cards
{
    public static class RankExtensionMethods
    {
        public static Card Of(this Rank rank, Suit suit)
        {
            return new Card(rank, suit);
        }
    }
}