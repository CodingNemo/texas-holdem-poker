﻿using System;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Framework;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Games.Exceptions;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Games
{
    public class Game : Aggregate,
        IHandle<StartGame>,
        IHandle<PlayerFolds>,
        IHandle<PlayerBets>,
        IHandle<PlayerRaises>,
        IHandle<PlayerGoesAllIn>,
        IHandle<PlayerCalls>,
        IHandle<PlayerChecks>
    {
        private GameState State => Get<GameState>();

        private class GameState : StateProjection
        {
            public BettingRound Round(BettingRoundName name, Action<DomainEvent> fire)
            {
                switch (name)
                {
                    case Preflop:
                        return new PreflopRound(
                            Id, Deck, Blinds,
                            Bets,
                            DealerId, Players,
                            fire);
                    case Flop:
                        return new FlopRound(
                            Id, Deck,
                            Bets,
                            DealerId, Players,
                            fire);
                    case Turn:
                        return new TurnRound(
                            Id, Deck,
                            Bets,
                            DealerId, Players,
                            fire);
                    case River:
                        return new RiverRound(
                            Id, Deck,
                            Bets,
                            DealerId, Players,
                            fire);
                    case Showdown:
                        return new ShowdownRound(
                            Id, Deck,
                            Bets, Board, Hands, Pots,
                            DealerId, Players, fire);
                    default:
                        throw new Exception($"UNKNOWN BETTING ROUND {CurrentBettingRound}");
                }
            }

            public GameState ValidatePlayerTurn(PlayerId playerId)
            {
                if (playerId != Players.CurrentId)
                {
                    throw new NotYourTurn("Don't rush...");
                }

                return this;
            }

            public GameState ValidateCheck()
            {
                if (CurrentBettingRound == Preflop)
                {
                    ValidatePreflopCheck();
                }
                else if (Bets.LastBet != null)
                {
                    throw new InvalidCheck("You can't check when someone bet or raised before you. Consider Call, Raise or Fold.");
                }

                return this;
            }

            private void ValidatePreflopCheck()
            {
                void ValidateOnlyBigBlingCanCheckDuringPreflop(PlayerId bigBlindPlayerId)
                {
                    if (Players.CurrentId != bigBlindPlayerId)
                    {
                        throw new InvalidCheck("This is the Preflop. Only the BigBlind can Check. Consider Call, Raise or Fold.");
                    }
                }

                void ValidateBigBlindCanCheckOnlyIfNobodyRaised(PlayerId bigBlindPlayerId)
                {
                    var allPlayersButBigBlind =
                    Players.Except(new[] { bigBlindPlayerId });

                    var aPlayerHasRaised = allPlayersButBigBlind.Select(pid => Players[pid])
                                         .Any(p => p.LastMove == PlayerMoveType.Raise
                                         || p.LastMove == PlayerMoveType.AllIn);

                    if (aPlayerHasRaised)
                    {
                        throw new InvalidCheck("You can't check when someone bet or raised before you. Consider Call, Raise or Fold.");
                    }
                }

                var bigBlindPlayer = Players.After(Players.After(DealerId));

                ValidateOnlyBigBlingCanCheckDuringPreflop(bigBlindPlayer);

                ValidateBigBlindCanCheckOnlyIfNobodyRaised(bigBlindPlayer);
            }

            public GameState ValidateCall()
            {
                if (Bets.LastBet == null)
                {
                    throw new InvalidCall("You can't call when nobody before you bet or raise");
                }

                return this;
            }

            public GameState ValidateBet(PlayerId playerId, int betValue)
            {
                if (betValue == 0)
                {
                    throw new InvalidBet($"C'mon ! You can't bet {betValue}");
                }
                if (betValue < 0)
                {
                    throw new InvalidBet($"Even though negative matter exists, there is no such thing as negative chips. You can't bet {betValue}");
                }
                if (betValue > Players[playerId].ChipsValue)
                {
                    throw new InvalidBet($"You can't bet more chips than you own : {betValue} > {Players[playerId].ChipsValue}. Consider the {nameof(PlayerAllIn)} command.");
                }

                return this;
            }

            public GameState ValidateRaise(PlayerId playerId, int raiseValue)
            {
                if (Bets.LastBet == null)
                {
                    throw new InvalidRaise("You can't raise when nobody before you bet");
                }

                if (Bets.LastBet.Value + raiseValue > Players[playerId].ChipsValue)
                {
                    throw new InvalidRaise($"You can't raise more than you own : {Bets.LastBet.Value} + {raiseValue} > {Players[playerId].ChipsValue}. Consider the {nameof(PlayerAllIn)} command.");
                }

                if (raiseValue < Bets.LastRaise)
                {
                    throw new InvalidRaise($"You must, at least, raise more than the last raise : {raiseValue} < {Bets.LastRaise}. Don't be cheap !!");
                }

                return this;
            }

            private GameId Id { get; set; }
            private PlayerId DealerId { get; set; }
            private Deck Deck { get; set; }
            private Blinds Blinds { get; set; }
            private Bets Bets { get; set; }
            private InGameHands Hands { get; set; }
            private Board Board { get; set; }
            private Pots Pots { get; set; }
            private InGamePlayers Players { get; set; }

            public BettingRoundName CurrentBettingRound { get; set; }

            public GameState()
            {
                Register<GameIsStarted>(When);
                Register<BettingRoundStarted>(When);
                Register<BettingRoundIsOver>(When);
                Register<CardDealtToPlayer>(When);
                Register<CardBurnt>(When);
                Register<FlopDealt>(When);
                Register<TurnDealt>(When);
                Register<RiverDealt>(When);
                Register<PlayerTurnStarted>(When);
                Register<BlindPosted>(When);
                Register<PlayerBet>(When);
                Register<PlayerRaised>(When);
                Register<PlayerAllIn>(When);
                Register<PlayerFolded>(When);
                Register<PlayerCalled>(When);
                Register<PlayerChecked>(When);
            }

            private void When(GameIsStarted gameStarted)
            {
                Id = gameStarted.GameId;
                Blinds = gameStarted.Blinds;
                Players = new InGamePlayers(gameStarted.Players.Select(kvp => new InGamePlayer(kvp.Key, kvp.Value)));
                Deck = new Deck(gameStarted.Cards);
                DealerId = gameStarted.DealerId;
                Board = new Board();
                Hands = new InGameHands();
                Pots = new Pots();
                Bets = new Bets();
            }

            private void When(PlayerFolded playerFolded)
            {
                Players[playerFolded.PlayerId].Fold(CurrentBettingRound);
            }

            private void When(PlayerCalled playerCalled)
            {
                Bets.Place(Bet.New(playerCalled.PlayerId, Bets.LastBet.Value));
                Players[playerCalled.PlayerId].Call(CurrentBettingRound);
            }

            private void When(PlayerChecked playerChecked)
            {
                Players[playerChecked.PlayerId].Check(CurrentBettingRound);
            }

            private void When(BlindPosted blindPosted)
            {
                Bets.Place(Bet.New(blindPosted.PlayerId, blindPosted.BlindValue));
            }

            private void When(PlayerBet playerBet)
            {
                Bets.Place(Bet.New(playerBet.PlayerId, playerBet.BetValue));
                Players[playerBet.PlayerId].Bet(CurrentBettingRound);
            }

            private void When(PlayerRaised playerRaised)
            {
                var lastBet = Bets.LastBet;
                Bets.Place(Bet.New(playerRaised.PlayerId, lastBet.Value + playerRaised.RaiseValue));
                Players[playerRaised.PlayerId].Raise(CurrentBettingRound);
            }

            private void When(PlayerAllIn playerAllIn)
            {
                var player = Players[playerAllIn.PlayerId];
                var chips = player.ChipsValue;
                Bets.Place(Bet.New(playerAllIn.PlayerId, chips));
                player.AllIn(CurrentBettingRound);
            }

            private void When(BettingRoundStarted bettingRoundStarted)
            {
                CurrentBettingRound = bettingRoundStarted.Name;
                Players.CurrentId = bettingRoundStarted.StartsAfter;
                Bets.Clean();
            }

            private void When(BettingRoundIsOver bettingRoundIsOver)
            {
                foreach (var bet in bettingRoundIsOver.Bets)
                {
                    var player = Players[bet.Key];
                    player.Pay(bet.Value);

                    if (player.LastMove == PlayerMoveType.AllIn)
                    {
                        Pots.AddLastContribution(bet.Key, bet.Value);
                    }
                    else
                    {
                        Pots.AddContribution(bet.Key, bet.Value);
                    }
                }
            }

            private void When(CardDealtToPlayer cardDealtToPlayer)
            {
                Deck.DrawTopCard(cardDealtToPlayer.Card);
                Hands[cardDealtToPlayer.Player].AddCard(cardDealtToPlayer.Card);
            }

            private void When(CardBurnt cardBurnt)
            {
                Deck.DrawTopCard(cardBurnt.Card);
            }

            private void When(FlopDealt flopDealt)
            {
                flopDealt.Cards.ForEach(card =>
                {
                    Deck.DrawTopCard(card);
                    Board.Add(card);
                });
            }

            private void When(TurnDealt turnDealt)
            {
                Deck.DrawTopCard(turnDealt.TurnCard);
                Board.Add(turnDealt.TurnCard);
            }

            private void When(RiverDealt riverDealt)
            {
                Deck.DrawTopCard(riverDealt.RiverCard);
                Board.Add(riverDealt.RiverCard);
            }

            private void When(PlayerTurnStarted playerTurnStarted)
            {
                Players.CurrentId = playerTurnStarted.PlayerId;
            }
        }

        public Game(History history, ILogEvents log)
            : base(history, log, new GameState()) { }

        public void Handle(StartGame command)
        {
            Fire(new GameIsStarted(command.GameId, command.Cards, command.Blinds, command.DealerId, command.Players.ToDictionary(p => p.Id, p => p.ChipsValue)));

            State.Round(Preflop, Fire)
                 .Continue();
        }

        public void Handle(PlayerBets command)
        {
            State.ValidatePlayerTurn(command.PlayerId)
                 .ValidateBet(command.PlayerId, command.BetValue);

            Fire(new PlayerBet(
                    command.AggregateId, command.PlayerId, command.BetValue));

            ContinueRound();
        }

        public void Handle(PlayerRaises command)
        {
            State.ValidatePlayerTurn(command.PlayerId)
                 .ValidateRaise(command.PlayerId, command.RaiseValue);

            Fire(new PlayerRaised(
                    command.AggregateId, command.PlayerId, command.RaiseValue));

            ContinueRound();
        }

        public void Handle(PlayerGoesAllIn command)
        {
            State.ValidatePlayerTurn(command.PlayerId);

            Fire(new PlayerAllIn(command.AggregateId, command.PlayerId));

            ContinueRound();
        }

        public void Handle(PlayerChecks command)
        {
            State.ValidatePlayerTurn(command.PlayerId)
                 .ValidateCheck();

            Fire(new PlayerChecked(command.AggregateId, command.PlayerId));

            ContinueRound();
        }

        public void Handle(PlayerCalls command)
        {
            State.ValidatePlayerTurn(command.PlayerId)
                 .ValidateCall();

            Fire(new PlayerCalled(command.AggregateId, command.PlayerId));

            ContinueRound();
        }

        public void Handle(PlayerFolds command)
        {
            State.ValidatePlayerTurn(command.PlayerId);

            Fire(new PlayerFolded(command.AggregateId, command.PlayerId));

            ContinueRound();
        }

        private void ContinueRound()
        {
            ContinueRound(State.CurrentBettingRound);
        }

        private void ContinueRound(BettingRoundName roundName)
        {
            if (roundName == None)
            {
                return;
            }

            var round = State.Round(roundName, Fire);
            var roundStatus = round.Continue();

            if (roundStatus == BettingRoundStatus.Over)
            {
                ContinueRound(round.NextBettingRoundName);
            }
        }
    }
}