﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Games.Domain
{
    public interface IEnumeratePlayerIds : IEnumerable<PlayerId>
    {
        InGamePlayer this[PlayerId playerId] { get; }
        int Count { get; }
        PlayerId After(PlayerId player);
        PlayerId NextId { get; }
        IEnumerable<PlayerId> StartsAfter(PlayerId playerId);
    }
}