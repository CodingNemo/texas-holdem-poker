﻿using System.Collections.Generic;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using Value;

namespace TexasHoldemPoker.Games.Domain
{
    public class PlayerMove : ValueType<PlayerMove>
    {
        private class NoPlayerMove : PlayerMove
        {
            public NoPlayerMove()
                : base(PlayerMoveType.None, BettingRoundName.None) { }
        }

        public static PlayerMove None => new NoPlayerMove();

        public PlayerMoveType Type { get; }
        public BettingRoundName Round { get; }

        public PlayerMove(PlayerMoveType type, BettingRoundName round)
        {
            Type = type;
            Round = round;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new object[] { Type, Round };
        }

        public override string ToString()
        {
            return $"{Round}-{Type}";
        }


    }
}