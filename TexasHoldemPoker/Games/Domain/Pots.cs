﻿using System.Collections;
using System.Collections.Generic;
using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Games.Domain
{
    public class Pots : IEnumerable<Pot>
    {
        private readonly LinkedList<Pot> pots = new LinkedList<Pot>();

        public Pot Current { get; private set; }

        public Pots()
        {
            var firstPot = new Pot();
            pots.AddFirst(firstPot);
            Current = firstPot;
        }

        public IEnumerator<Pot> GetEnumerator()
        {
            return pots.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void AddLastContribution(PlayerId player, int contribution)
        {
            ContributeToPot(player, contribution, true);
        }

        public void AddContribution(PlayerId player, int contribution)
        {
            ContributeToPot(player, contribution, false);
        }

        private void ContributeToPot(PlayerId player, int value, bool setLimit)
        {
            ContributeToPot(player, value, pots.Find(Current), setLimit);
        }

        private void ContributeToPot(PlayerId player, int value, LinkedListNode<Pot> node, bool setLimit)
        {
            var remaining = node.Value.Contribute(player, value, setLimit);

            if (remaining > 0)
            {
                if (node.Next == null)
                {
                    pots.AddAfter(node, new Pot());
                }

                ContributeToPot(player, remaining, node.Next, setLimit);
            }
        }
    }
}
