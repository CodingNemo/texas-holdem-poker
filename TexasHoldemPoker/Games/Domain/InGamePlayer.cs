﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Players;
using Value;

namespace TexasHoldemPoker.Games.Domain
{
    public class InGamePlayer : ValueType<InGamePlayer>
    {
        private readonly Stack<PlayerMove> _moves = new Stack<PlayerMove>();

        public PlayerId Id { get; }
        public int ChipsValue { get; private set; }
        public PlayerMoveType LastMove => PeekLastMove().Type;

        private static readonly PlayerMoveType[] finalPlayerMoves = new[]
                {
                    PlayerMoveType.Fold,
                    PlayerMoveType.AllIn
                };

        public bool CanMove
        {
            get
            {
                var lastMove = PeekLastMove();
                return !finalPlayerMoves.Contains(lastMove.Type);
            }
        }

        private PlayerMove PeekLastMove()
        {
            if (!_moves.Any())
            {
                return PlayerMove.None;
            }

            return _moves.Peek();
        }

        public InGamePlayer(PlayerId id, int chipsValue)
        {
            Id = id;
            ChipsValue = chipsValue;
        }

        public override string ToString()
        {
            string lastMove = PeekLastMove() == PlayerMove.None ? "" : PeekLastMove().ToString();
            return $"{Id}{lastMove}[{ChipsValue}]";
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new object[] { Id };
        }

        public void Fold(BettingRoundName round)
        {
            Move(PlayerMoveType.Fold, round);
        }

        public void Check(BettingRoundName round)
        {
            Move(PlayerMoveType.Check, round);
        }

        public void Bet(BettingRoundName round)
        {
            Move(PlayerMoveType.Bet, round);
        }

        public void Call(BettingRoundName round)
        {
            Move(PlayerMoveType.Call, round);
        }

        public void Raise(BettingRoundName name)
        {
            Move(PlayerMoveType.Raise, name);
        }

        public void AllIn(BettingRoundName name)
        {
            Move(PlayerMoveType.AllIn, name);
        }

        public bool HasMoved(BettingRoundName bettingRound)
        {
            return PeekLastMove().Round == bettingRound;
        }

        private void Move(PlayerMoveType moveType, BettingRoundName round)
        {
            _moves.Push(new PlayerMove(moveType, round));
        }

        public void Pay(int value)
        {
            ChipsValue -= value;
        }
    }
}