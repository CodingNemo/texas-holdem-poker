﻿namespace TexasHoldemPoker.Games.Domain
{
    public enum PlayerMoveType
    {
        None,
        Fold,
        Check, Call,
        Bet, Raise, AllIn
    }
}