﻿using System;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Framework;
using static TexasHoldemPoker.Games.Domain.PlayerMoveType;
using static ActionExtensionMethods;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Games.Domain.BettingRounds;

namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    public class PreflopRound : BettingRound
    {
        private readonly Blinds blinds;
        private PlayerId SmallBlindPlayerId
        {
            get
            {
                if (Players.Count == 2)
                {
                    return DealerId;
                }

                return Players.After(DealerId);
            }
        }
        private PlayerId BigBlindPlayerId => Players.After(SmallBlindPlayerId);

        public override BettingRoundName NextBettingRoundName => BettingRoundName.Flop;
        public override BettingRoundName CurrentBettingRoundName => BettingRoundName.Preflop;

        public PreflopRound(GameId gameId, Deck deck, Blinds blinds, Bets bets, PlayerId dealerId, IEnumeratePlayerIds players, Action<DomainEvent> fire)
            : base(gameId, deck, bets, dealerId, players, fire)
        {
            this.blinds = blinds;
        }

        protected override void StartBettingRound()
        {
            Fire(new BettingRoundStarted(GameId, BettingRoundName.Preflop, BigBlindPlayerId));
        }

        protected override void OnStart()
        {
            DealCards(2);
            PostBlinds();
        }

        private void PostBlinds()
        {
            Fire(new BlindPosted(
                GameId, SmallBlindPlayerId, blinds.Small));

            Fire(new BlindPosted(
                GameId, BigBlindPlayerId, blinds.Big));
        }

        protected override bool IsOver()
        {
            return base.IsOver() ||
                AllPlayerCalledAndBigBindChecked();
        }

        private bool AllPlayerCalledAndBigBindChecked()
        {
            return Players.Except(new[] { BigBlindPlayerId })
                          .All(pid =>
                          {
                              var p = Players[pid];
                              return
                              p.LastMove == Fold ||
                              p.LastMove == Call;
                          })
                && Players[BigBlindPlayerId].LastMove == Check;
        }

        private void DealCards(int perPlayer)
        {
            var cards = Deck.Tops(count: perPlayer * Players.Count).ToArray();
            var dealtCardIndex = 0;

            Repeat(perPlayer, () =>
            {
                foreach (var player in Players.StartsAfter(DealerId))
                {
                    Fire(new CardDealtToPlayer(GameId, player, cards[dealtCardIndex]));
                    dealtCardIndex++;
                }
            });
        }
    }
}