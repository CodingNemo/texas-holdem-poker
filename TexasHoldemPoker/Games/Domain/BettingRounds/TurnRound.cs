﻿using System;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    internal class TurnRound : BettingRound
    {
        public TurnRound(
            GameId gameId, Deck deck, Bets bets,
            PlayerId dealerId, IEnumeratePlayerIds players,
            Action<DomainEvent> fire)
            : base(gameId, deck, bets, dealerId, players, fire)
        {
        }

        public override BettingRoundName NextBettingRoundName => BettingRoundName.River;

        public override BettingRoundName CurrentBettingRoundName => BettingRoundName.Turn;

        protected override void OnStart()
        {
            DealTurnCard();
        }

        private void DealTurnCard()
        {
            Fire(new CardBurnt(GameId, Deck.Top));
            Fire(new TurnDealt(GameId, Deck.Top));
        }
    }
}