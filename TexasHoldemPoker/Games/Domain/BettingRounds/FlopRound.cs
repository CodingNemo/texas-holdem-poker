﻿using System;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    public class FlopRound : BettingRound
    {
        public override BettingRoundName NextBettingRoundName => Turn;
        public override BettingRoundName CurrentBettingRoundName => Flop;

        public FlopRound(GameId gameId, Deck deck, Bets bets, PlayerId dealerId, IEnumeratePlayerIds players, Action<DomainEvent> fire)
        : base(gameId, deck, bets, dealerId, players, fire)
        {
        }

        protected override void OnStart()
        {
            DrawFlop();
        }
        
        private void DrawFlop()
        {
            Fire(new CardBurnt(GameId, Deck.Top));
            Fire(new FlopDealt(GameId, Deck.Tops(3).ToArray()));
        }
    }
}