﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Players;

using static TexasHoldemPoker.Games.Domain.PlayerMoveType;

namespace TexasHoldemPoker.Games.Domain
{
    public class InGamePlayers : IEnumeratePlayerIds
    {
        private readonly List<PlayerId> playerIds;
        private readonly Dictionary<PlayerId, InGamePlayer> playersByIds;

        public InGamePlayers(IEnumerable<InGamePlayer> players)
            : this(players.ToList()) { }

        private InGamePlayers(IReadOnlyCollection<InGamePlayer> players)
        {
            playerIds = players.Select(p => p.Id).ToList();
            playersByIds = players.GroupBy(p => p.Id)
                .ToDictionary(g => g.Key, g => g.Single());
        }

        public InGamePlayer this[PlayerId playerId] => playersByIds[playerId];

        public PlayerId After(PlayerId player)
        {
            var playerIndex = playerIds.IndexOf(player);
            var nextPlayerIndex = NextIndex(playerIndex);
            var nextPlayerId = playerIds[nextPlayerIndex];
            var nextPlayer = this[nextPlayerId];

            return nextPlayer.CanMove ?
                nextPlayerId : After(nextPlayerId);
        }

        public PlayerId CurrentId { get; set; }
        public PlayerId NextId => After(CurrentId);
        public int Count => playerIds.Count;

        private int NextIndex(int index)
        {
            var nextIndex = index + 1;
            if (nextIndex >= Count)
            {
                nextIndex = 0;
            }

            return nextIndex;
        }

        public IEnumerator<PlayerId> GetEnumerator()
        {
            return playerIds.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerable<PlayerId> StartsAfter(PlayerId playerId)
        {
            var newPlayers = new List<PlayerId>();
            var currentPlayerId = After(playerId);

            while (currentPlayerId != playerId)
            {
                newPlayers.Add(currentPlayerId);
                currentPlayerId = After(currentPlayerId);
            }

            newPlayers.Add(playerId);
            return newPlayers.ToArray();
        }
    }
}