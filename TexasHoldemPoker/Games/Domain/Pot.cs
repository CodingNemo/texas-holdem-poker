﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Games.Domain
{
    public class Pot
    {
        public int Id { get; private set; }

        public int Value { get; private set; }

        private readonly HashSet<PlayerId> contributors = new HashSet<PlayerId>();
        public IEnumerable<PlayerId> Contributors => contributors;

        public bool HasLimit { get; private set; }
        public int Limit { get; private set; }

        public int Contribute(PlayerId contributor, int value, bool setLimit)
        {
            var remaining = 0;
            var contribution = value;
            if (HasLimit && value >= Limit)
            {
                contribution = Limit;
                remaining = value - contribution;
            }
            else if (setLimit)
            {
                SetLimit(value);
            }

            Value += contribution;
            contributors.Add(contributor);

            return remaining;
        }

        public void SetLimit(int limit)
        {
            HasLimit = true;
            Limit = limit;
        }
    }
}