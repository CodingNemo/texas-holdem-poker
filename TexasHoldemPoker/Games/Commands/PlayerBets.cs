﻿using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Commands
{
    public class PlayerBets : Do<Game>
    {
        public object AggregateId { get; }
        public PlayerId PlayerId { get; }
        public int BetValue { get; }

        public PlayerBets(GameId gameId, PlayerId playerId, int betValue)
        {
            AggregateId = gameId;
            PlayerId = playerId;
            BetValue = betValue;
        }
    }
}