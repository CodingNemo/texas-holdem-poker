﻿using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Commands
{
    public class PlayerFolds : Do<Game>
    {
        public PlayerId PlayerId { get; }
        public object AggregateId { get; }


        public PlayerFolds(GameId gameId, PlayerId playerId)
        {
            AggregateId = gameId;
            PlayerId = playerId;
        }
    }
}