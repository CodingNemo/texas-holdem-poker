﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;

namespace TexasHoldemPoker.Games.Events
{
    public class FlopDealt : DomainEvent<FlopDealt>
    {
        public override object AggregateId { get; }
        public Card[] Cards { get; }

        public FlopDealt(GameId gameId, Card[] cards)
        {
            AggregateId = gameId;
            Cards = cards;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { new ListByValue<Card>(Cards) });
        }
    }
}