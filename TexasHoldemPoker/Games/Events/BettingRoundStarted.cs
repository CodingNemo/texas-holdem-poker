﻿using System.Collections.Generic;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using System.Linq;
using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Games.Events
{
    public class BettingRoundStarted : DomainEvent<BettingRoundStarted>
    {
        public override object AggregateId { get; }
        public BettingRoundName Name { get; }
        public PlayerId StartsAfter { get; }

        public BettingRoundStarted(GameId gameId, BettingRoundName name, PlayerId startsAfter)
        {
            AggregateId = gameId;
            Name = name;
            StartsAfter = startsAfter;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { Name });
        }
    }
}