﻿using System.Collections.Generic;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class CardDealtToPlayer : DomainEvent<CardDealtToPlayer>
    {
        public Card Card { get; }

        public PlayerId Player { get; }

        public override object AggregateId { get; }

        public CardDealtToPlayer(GameId gameId, PlayerId player, Card card)
        {
            AggregateId = gameId;
            Card = card;
            Player = player;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { Card, Player });
        }
    }
}