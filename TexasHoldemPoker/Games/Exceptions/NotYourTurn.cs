﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Games.Exceptions
{
    [Serializable]
    public class NotYourTurn : Exception
    {
        public NotYourTurn()
        {
        }

        public NotYourTurn(string message) : base(message)
        {
        }

        public NotYourTurn(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotYourTurn(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}