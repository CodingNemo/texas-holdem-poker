﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Games.Exceptions
{
    [Serializable]
    public class InvalidCall : Exception
    {
        public InvalidCall()
        {
        }

        public InvalidCall(string message) : base(message)
        {
        }

        public InvalidCall(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidCall(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
