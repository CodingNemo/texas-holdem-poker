﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tables.Events
{
    public class TableOpened : DomainEvent<TableOpened>
    {
        public override object AggregateId { get; }

        public TableId TableId { get; }

        public int NumberOfSeats { get; }

        public decimal SmallBlindAmount { get; }
        public decimal BigBlindAmount { get; }

        public TableOpened(TableId tableId, int numberOfSeats, decimal smallBlindAmount, decimal bigBlindAmount)
        {
            AggregateId = tableId;
            TableId = tableId;
            NumberOfSeats = numberOfSeats;
            SmallBlindAmount = smallBlindAmount;
            BigBlindAmount = bigBlindAmount;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality().Union(new object[]
            {
                NumberOfSeats, SmallBlindAmount, BigBlindAmount
            });
        }
    }
}