﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Framework;
using Value;
using System.Linq;

namespace TexasHoldemPoker.Tables.Events
{
    public class PlayerJoined : DomainEvent<PlayerJoined>
    {
        public override object AggregateId { get; }

        public PlayerId PlayerId { get; }

        public decimal Stack { get; }

        public PlayerJoined(TableId tableId, PlayerId playerId, decimal stack)
        {
            AggregateId = tableId;
            PlayerId = playerId;
            Stack = stack;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality().Union(new object[] {PlayerId, Stack });
        }
    }
}