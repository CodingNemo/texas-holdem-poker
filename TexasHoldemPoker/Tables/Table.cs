﻿using System.Linq;
using TexasHoldemPoker.Framework;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables.Commands;
using TexasHoldemPoker.Tables.Events;
using TexasHoldemPoker.Tables.Exceptions;

namespace TexasHoldemPoker.Tables
{
    public class Table : Aggregate,
        IHandle<PlayerJoins>,
        IHandle<OpenNewTable>
    {
        private TableState State => Get<TableState>();

        private class TableState : StateProjection
        {
            private GameId gameIdSeed;
            private TableId id;

            private int numberOfSeats;

            public TableState()
            {
                Register<TableOpened>(When);
                Register<PlayerJoined>(When);
            }

            public Loop<SeatedPlayer> SeatedPlayers { get; private set; }
            public decimal SmallBlindAmount { get; private set; }
            public decimal BigBlindAmount { get; private set; }

            private bool TableIsFull => numberOfSeats == SeatedPlayers.Count;

            internal void PlayerCanJoin(PlayerId playerId)
            {
                if (PlayerIsSeated(playerId))
                {
                    throw new PlayerAlreadySeated(playerId);
                }

                if (TableIsFull)
                {
                    throw new TableIsFull();
                }
            }

            private void When(TableOpened evt)
            {
                id = evt.TableId;
                numberOfSeats = evt.NumberOfSeats;
                SeatedPlayers = new Loop<SeatedPlayer>(numberOfSeats);
                SmallBlindAmount = evt.SmallBlindAmount;
                BigBlindAmount = evt.BigBlindAmount;
            }

            private void When(PlayerJoined evt)
            {
                SeatedPlayers.Add(new SeatedPlayer(evt.PlayerId, evt.Stack));
            }

            private bool PlayerIsSeated(PlayerId playerId)
            {
                return SeatedPlayers.Any(sp => sp.PlayerId == playerId);
            }

            public GameId NextGameId()
            {
                return gameIdSeed == null ?
                    GameId.New(id, 0)
                    : gameIdSeed.Next();
            }
        }

        public Table(History history, ILogEvents log)
            :base(history, log, new TableState())
        {
        }

        public void Handle(OpenNewTable command)
        {
            Fire(new TableOpened(
                command.TableId,
                command.NumberOfSeats,
                command.SmallBlindAmount,
                command.BigBlindAmount));
        }

        public void Handle(PlayerJoins command)
        {
            State.PlayerCanJoin(command.PlayerId);

            Fire(
                new PlayerJoined(
                    command.TableId,
                    command.PlayerId,
                    command.Stack));
        }
    }
}