﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Tables.Exceptions
{
    [Serializable]
    public class NotEnoughPlayers : Exception
    {
        public NotEnoughPlayers(string message) : base(message)
        {
        }

        protected NotEnoughPlayers(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}