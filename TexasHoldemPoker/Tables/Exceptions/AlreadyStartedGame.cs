﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Tables.Exceptions
{
    [Serializable]
    public class AlreadyStartedGame : Exception
    {
        public AlreadyStartedGame(string message) : base(message)
        {
        }

        protected AlreadyStartedGame(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}