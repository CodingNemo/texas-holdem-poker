﻿using System;
using System.Runtime.Serialization;
using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Tables.Exceptions
{
    [Serializable]
    public class PlayerAlreadySeated : Exception
    {
        public PlayerAlreadySeated(PlayerId playerId)
            : base($"PlayerId {playerId} already in game")
        {
        }

        protected PlayerAlreadySeated(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}