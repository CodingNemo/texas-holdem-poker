﻿using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands.HandValues
{
    public class ThreeOfAKind : CardGroupsHandValue
    {
        private const long THREE_OF_A_KIND_VALUE = 3000000;

        public ThreeOfAKind(Hand hand, Rank rank, HighCard remainingCards)
            : base(hand, rank, remainingCards, 3) { }

        internal override long ValueStrength => THREE_OF_A_KIND_VALUE;
    }
}