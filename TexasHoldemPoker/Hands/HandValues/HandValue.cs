﻿using System;
using System.Collections.Generic;
using Value;

namespace TexasHoldemPoker.Hands.HandValues
{
    public abstract class HandValue : ValueType<HandValue>, IComparable<HandValue>
    {
        private long? strength;

        protected HandValue(Hand hand)
        {
            Hand = hand;
        }

        public Hand Hand { get; }

        internal long FullStrength
        {
            get
            {
                if (!strength.HasValue)
                {
                    strength = ValueStrength + ComputeHandStrength();
                }

                return strength.Value;
            }
        }

        internal abstract long ValueStrength { get; }

        protected abstract long ComputeHandStrength();

        public override int GetHashCode()
        {
            return Hand.GetHashCode();
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new object[] { FullStrength };
        }

        public override string ToString()
        {
            return $"{Hand} ({FullStrength}) = {GetType().Name}";
        }

        public int CompareTo(HandValue other)
        {
            if(this.Beats(other))
            {
                return 1;
            }

            if(other.Beats(this))
            {
                return -1;
            }

            return 0;
        }
    }

    public static class HandValueExtensionMethods
    {
        public static bool Beats(this HandValue strongest, HandValue weakest)
        {
            if (strongest.ValueStrength > weakest.ValueStrength)
            {
                return true;
            }

            if (strongest.ValueStrength < weakest.ValueStrength)
            {
                return false;
            }

            return strongest.FullStrength > weakest.FullStrength;
        }
    }
}