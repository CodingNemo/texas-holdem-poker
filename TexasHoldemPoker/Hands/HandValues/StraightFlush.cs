﻿namespace TexasHoldemPoker.Hands.HandValues
{
    public class StraightFlush : HandValue
    {
        private const long STRAIGHT_FLUSH_VALUE = 8000000;

        public StraightFlush(Hand hand, Straight straight, Flush flush)
            : base(hand)
        {
            Straight = straight;
            Flush = flush;
        }

        public Straight Straight { get; }

        public Flush Flush { get; }

        internal override long ValueStrength => STRAIGHT_FLUSH_VALUE;

        protected override long ComputeHandStrength()
        {
            var straightRankAsLong = (long) Straight.HighestCard.Rank;
            const long straightRankFactor = 14;
            var flushRankAsLong = (long) Flush.Suit;
            const long flushRankFactor = 5;

            return straightRankAsLong * straightRankFactor + flushRankAsLong * flushRankFactor;
        }

        public override string ToString()
        {
            return base.ToString() + $" of {Flush.Suit} and {Straight.HighestCard.Rank} High ";
        }
    }
}