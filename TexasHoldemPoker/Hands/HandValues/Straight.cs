﻿using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands.HandValues
{
    public class Straight : HandValue
    {
        private const long STRAIGHT_VALUE = 4000000;

        public Straight(Hand hand, Card highestCard)
            : base(hand)
        {
            HighestCard = highestCard;
        }

        public Card HighestCard { get; }

        internal override long ValueStrength => STRAIGHT_VALUE;

        protected override long ComputeHandStrength()
        {
            return (long) HighestCard.Rank;
        }

        public override string ToString()
        {
            return base.ToString() + $" with {HighestCard} High";
        }
    }
}