﻿using System;

namespace TexasHoldemPoker.Hands.HandValues
{
    public class TwoPairs : HandValue
    {
        private const long TWO_PAIRS_VALUES = 2000000;

        public TwoPairs(Hand hand, Pair firstPair, Pair secondPair, HighCard remainingCard)
            : base(hand)
        {
            if (firstPair.Rank > secondPair.Rank)
            {
                HighestPair = firstPair;
                LowestPair = secondPair;
            }
            else
            {
                HighestPair = secondPair;
                LowestPair = firstPair;
            }

            RemainingCard = remainingCard;
        }

        public Pair HighestPair { get; }
        public Pair LowestPair { get; }

        public HighCard RemainingCard { get; }

        internal override long ValueStrength => TWO_PAIRS_VALUES;

        protected override long ComputeHandStrength()
        {
            var highestPairRankAsLong = (long) HighestPair.Rank;
            var highestPairFactor = (long) Math.Pow(14, 2);

            var lowestPairRankAsLong = (long) LowestPair.Rank;
            long lowestPairFactor = 14;

            var remainingCardStrength = RemainingCard.FullStrength;

            return
                highestPairRankAsLong * highestPairFactor +
                lowestPairRankAsLong * lowestPairFactor +
                remainingCardStrength;
        }

        public override string ToString()
        {
            return base.ToString() + $" of {HighestPair.Rank} And {LowestPair.Rank}";
        }
    }
}